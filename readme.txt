﻿Чтобы собрать приложение необходимо:
Установить Visual Studio 2015
Установить Qt https://www.qt.io/ru/download/, при установке необходимо выбрать инструменты сборки Visual Studio 2015 x64 
Установить CMake c официального сайта https://cmake.org/
Установить OpenCV версии 3.1.0 !!! http://opencv.org/releases.html
Исправить файл build.bat в папке appcode/build, а именно:
1) указать путь к папке Cmake/bin (например: set PATH="C:/Program Files/CMake/bin")
2) указать путь к исполнимому файлу Cmake (например: "C:/Program Files/CMake/bin/cmake.exe")
3) указать путь к папке opencv (например: -DOpenCV_DIR="C:/devel/opencv/build")

Запустить скрипт build.bat
Открыть проект Misistagram.pro из папки appgui
Собрать и запустить приложение