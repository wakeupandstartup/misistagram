#include "imagemanager.h"
#include "QMessageBox"
#define DEBUG_IMAGEMANAGER 1



ImageManager::ImageManager() {
#if DEBUG_IMAGEMANAGER
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

    qRegisterMetaType<FILTER_IDS>("2");
    createBackEnd();

#if DEBUG_IMAGEMANAGER
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif
}

ImageManager::~ImageManager() {
#if DEBUG_IMAGEMANAGER
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

    releaseBackEnd();

#if DEBUG_IMAGEMANAGER
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif
}

QImage ImageManager::getCurrentImage() const {
#if DEBUG_IMAGEMANAGER
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

#if DEBUG_IMAGEMANAGER
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif

    return mCurrentImage.rgbSwapped();
}

void ImageManager::initializeImageData(ImageData &src, ImageData &dst) {
    dst.mWidth = src.mWidth;
    dst.mHeight = src.mHeight;
    dst.mBytesPerLine = src.mBytesPerLine;
    dst.mChannels = src.mChannels;
    dst.mCvFormat = src.mCvFormat;

    qDebug() << dst.mChannels;
    if (!dst.mData) {
        delete[] dst.mData;
    }
    dst.mData = nullptr;
    dst.mData = new uint8_t[dst.mChannels * dst.mWidth * dst.mHeight];
}

void ImageManager::createImageData(ImageData &imageData, QImage &qImage) {
    imageData.mWidth = qImage.width();
    imageData.mHeight = qImage.height();
    imageData.mData = qImage.bits();
    imageData.mBytesPerLine = qImage.bytesPerLine();
    imageData.mCvFormat = getCvFormatFromQt(qImage.format());
}

void ImageManager::createQImage(QImage &qImage, ImageData &imageData) {
    qDebug() << imageData.mCvFormat;
    qImage = QImage(imageData.mData, imageData.mWidth, imageData.mHeight, imageData.mBytesPerLine, getQtFormatFromCv(imageData.mCvFormat));
}

int ImageManager::getCvFormatFromQt(QImage::Format qFormat) const {
    switch (qFormat) {
    case QImage::Format_Indexed8:
    case QImage::Format_Grayscale8:
        return CV_8UC1;
    case QImage::Format_RGB888:
        return CV_8UC3;
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32:
        return CV_8UC4;
    default:
        return -1;
    }
}

QImage::Format ImageManager::getQtFormatFromCv(int cvFormat)  {
    switch (cvFormat) {
    case CV_8UC1:
        return QImage::Format_Grayscale8;
    case CV_8UC3:
        return QImage::Format_RGB888;
    case CV_8UC4:
        return QImage::Format_RGB32;
    default:
        return QImage::Format_Invalid;
    }
}

bool ImageManager::uploadImage() {
#if DEBUG_IMAGEMANAGER
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

    QString fileName = QFileDialog::getOpenFileName(nullptr, tr("Open Image"), QString(), tr("JPEG (*.jpg *.jpeg);;PNG (*.png)"));
    if (!fileName.isEmpty()) {
        QFile file(fileName);
        if (!file.open(QIODevice::ReadWrite)) {
            qWarning() << "Error. Could not open image file.";
        } else {
            mCurrentImage = mInputImage = mPrevImage = QImage(file.fileName()).convertToFormat(QImage::Format_RGB888).rgbSwapped();

            BrigtnessContrastParam param;
            param.brightness = 5;
            param.contrast = 50;

            BlurParam param2;
            param2.blurRadius = 5;

            for(int i = 0; i < 7; ++i) {
                applyFilter((FILTER_IDS)i, &param, false, false);

                mCurrentImage = QImage(mPrevImage);
            }

            emit sendProcessedImage(mCurrentImage.rgbSwapped());
        }
        return true;
    } else {
        qWarning() << "Error. Could not open image file.";
        return false;
    }

#if DEBUG_IMAGEMANAGER
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif
}

void ImageManager::saveImage() {
#if DEBUG_IMAGEMANAGER
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

    QString fileName = QFileDialog::getSaveFileName(nullptr, tr("Save Image"), QString(), tr("JPEG (*.jpg *.jpeg);;PNG (*.png)"));
    mCurrentImage.rgbSwapped().save(fileName);

#if DEBUG_IMAGEMANAGER
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif
}

void ImageManager::applyFilter(const int &filterId, void *parameters, const bool &isTotal, const bool &toHistory) {
#if DEBUG_IMAGEMANAGER
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

    if (isTotal == true) {
        if (toHistory) {
            undoHistory();
            addFilterToHistory((FILTER_IDS)filterId, parameters);
        }

        createImageData(mDesc, mCurrentImage);
        modifyImage(&mDesc, (FILTER_IDS)filterId, parameters);

        initializeImageData(mDesc, mDesc2);
        retrieveLastImage(&mDesc2);
        createQImage(mCurrentImage, mDesc2);

        if (mCurrentImage.format())
        emit sendProcessedImage(mCurrentImage.rgbSwapped());
        emit sendProcessedIcon(mCurrentImage.rgbSwapped(), filterId);
        mPrevImage = mCurrentImage;
    } else {
        createImageData(mDesc, mCurrentImage);
        modifyImage(&mDesc, (FILTER_IDS)filterId, parameters);

        initializeImageData(mDesc, mDesc2);
        retrieveLastImage(&mDesc2);
        createQImage(mCurrentImage, mDesc2);

        emit sendProcessedIcon(mCurrentImage.rgbSwapped(), filterId);
        emit sendProcessedImage(mCurrentImage.rgbSwapped());
        mCurrentImage = mPrevImage;
    }

#if DEBUG_IMAGEMANAGER
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif
}

void ImageManager::backward() {
    if (mCurrentHitoryPtr - 1 >= 0) {
        mCurrentImage = mPrevImage = mInputImage;
        mCurrentHitoryPtr--;
        uint32_t offset = 0;
        for (uint32_t i = 0; i < mCurrentHitoryPtr; ++i) {
            applyFilter(mOperations[i], mParameters.data() + offset, true, false);
            offset += getParameterSize(mOperations[i]);
        }
        emit sendProcessedImage(mCurrentImage.copy().rgbSwapped());
        return;
    }
}

void ImageManager::forward() {
    if (mCurrentHitoryPtr + 1 <= mOperations.size()) {
        mCurrentHitoryPtr++;
        mCurrentImage = mPrevImage = mInputImage;
        uint32_t offset = 0;
        for (uint32_t i = 0; i < mCurrentHitoryPtr; ++i) {
            applyFilter(mOperations[i], mParameters.data() + offset, true, false);
            offset += getParameterSize(mOperations[i]);
        }
        emit sendProcessedImage(mCurrentImage.copy().rgbSwapped());
        return;
    }
}

void ImageManager::addFilterToHistory(FILTER_IDS id, void* parameters)
{
    uint32_t size = getParameterSize(id);
    uint32_t oldSize = mParameters.size();
    mParameters.resize(oldSize + size);
    memcpy(mParameters.data() + oldSize, parameters, size);
    mOperations.push_back(id);
    mCurrentHitoryPtr++;
}

void ImageManager::undoHistory()
{
    for (int i = 0; i < mOperations.size() - mCurrentHitoryPtr; ++i) {
        uint32_t size = getParameterSize(mOperations.back());
        mParameters.resize(mParameters.size() - size);
        mOperations.pop_back();
    }
}

void ImageManager::reproduceFromInput()
{
    mCurrentImage = mPrevImage = mInputImage;
    uint32_t offset = 0;
    for (uint32_t i = 0; i < mOperations.size(); ++i)
    {
        applyFilter(mOperations[i], mParameters.data() + offset, true, false);
        offset += getParameterSize(mOperations[i]);
    }
}

struct MISIS_PRESET_MAGIC_HEADER
{
    uint8_t b1, b2, b3, b4;
    MISIS_PRESET_MAGIC_HEADER()
    {
        b1 = 0xff;
        b2 = 0xaf;
        b3 = 0x35;
        b4 = 0x12;
    }
};

void ImageManager::saveCurrentState(/*std::string& path*/)
{
    undoHistory();

    QString fileName = QFileDialog::getSaveFileName(nullptr, tr("Save current state"), QString(), tr("TXT (*.txt)"));
    QFile presetFile(fileName.toStdString().c_str());
    qDebug() << "fileName = " << fileName;
    if (!presetFile.open(QIODevice::WriteOnly)) {
        qDebug() << presetFile.error();
        return;
    }

    /***
      Write header
    */
    MISIS_PRESET_MAGIC_HEADER hdr;
    presetFile.write((const char*)(&hdr), sizeof(MISIS_PRESET_MAGIC_HEADER));

    uint32_t elemc = mOperations.size();
    uint32_t prsize = mParameters.size();

    presetFile.write((const char*)(&elemc), sizeof(uint32_t));
    presetFile.write((const char*)(&prsize), sizeof(uint32_t));

    presetFile.write((const char*)mOperations.data(), sizeof(uint32_t) * mOperations.size());
    presetFile.write((const char*)mParameters.data(), sizeof(uint8_t) * mParameters.size());

    presetFile.close();
}

void ImageManager::loadState(/*std::string& path*/)
{
    QString fileName = QFileDialog::getOpenFileName(nullptr, tr("Load state"), QString(), tr("TXT (*.txt)"));
    QFile presetFile(fileName.toStdString().c_str());
    qDebug() << "fileName = " << fileName;
    if (!presetFile.open(QIODevice::ReadOnly)) {
        qDebug() << presetFile.error();
        return;
    }
    /***
      Check header
    */
    MISIS_PRESET_MAGIC_HEADER hdr;
    MISIS_PRESET_MAGIC_HEADER trueHdr;
    presetFile.read((char*)(&hdr), sizeof(MISIS_PRESET_MAGIC_HEADER));

    if (hdr.b1 != trueHdr.b1 || hdr.b2 != trueHdr.b2 ||
            hdr.b3 != trueHdr.b3 || hdr.b4 != trueHdr.b4)
    {
        QMessageBox* message = new QMessageBox();
        message->setText("Preset file probably corrupted!");
        message->showNormal();
        return;
    }

    uint32_t elemc = 0;
    uint32_t prsize = 0;

    presetFile.read((char*)(&elemc), sizeof(uint32_t));
    presetFile.read((char*)(&prsize), sizeof(uint32_t));

    mOperations.clear();
    mParameters.clear();

    mOperations.resize(elemc);
    mParameters.resize(prsize);

    presetFile.read((char*)mOperations.data(), sizeof(uint32_t) * mOperations.size());
    presetFile.read((char*)mParameters.data(), sizeof(uint8_t) * mParameters.size());

    presetFile.close();

    qDebug() << "mCurrentHitoryPtr = " << mCurrentHitoryPtr;
    mCurrentHitoryPtr = mOperations.size();

    reproduceFromInput();
}

