#include "gui.h"
#include "ui_gui.h"
#include <iostream>

#define DEBUG_GUI 1

GUI::GUI(QWidget *parent) : QMainWindow(parent), ui(new Ui::GUI) {
#if DEBUG_GUI
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

    ui->setupUi(this);



    ui->sliderBlurRadius->setVisible(false);
    ui->sliderBrightness->setVisible(false);
    ui->sliderContrast->setVisible(false);

    ui->lblRadius->setVisible(false);
    ui->lblBrightness->setVisible(false);
    ui->lblContrast->setVisible(false);

    ui->btnGrayscaleFilter->setEnabled(false);
    ui->btnSepiaFilter->setEnabled(false);
    ui->btnHorizontalFlip->setEnabled(false);
    ui->btnVerticalFlip->setEnabled(false);
    ui->dialAngle->setEnabled(false);
    ui->btnApply->setEnabled(false);
    ui->btnForward->setEnabled(false);
    ui->btnBackward->setEnabled(false);
    ui->btnBlurFilter->setEnabled(false);
    ui->btnBrightnessContrstFilter->setEnabled(false);
    ui->btnNashvilleEffectFilter->setEnabled(false);
    ui->btnNineteenCenturyFilter->setEnabled(false);
    ui->btnVignetteFilter->setEnabled(false);
    ui->btnSave->setEnabled(false);
    ui->btnSavePreset->setEnabled(false);

    initStyleSheet(DARK);

    pImageManager = new ImageManager();

    pBlurParam = new BlurParam();
    pBrightnessContrastParam = new BrigtnessContrastParam();
    pRotateParam = new RotateParam();
    pFlippingParam = new FlippingParam();


    connect(pImageManager, &ImageManager::sendProcessedImage, this, &GUI::updateProcessedImage, Qt::QueuedConnection);
    connect(pImageManager, &ImageManager::sendProcessedIcon, this, &GUI::updateProcessedIcon, Qt::QueuedConnection);
    connect(this, &GUI::sendFilterId, pImageManager, &ImageManager::applyFilter);

    connect(ui->btnUpload, &QPushButton::clicked, this, &GUI::open, Qt::QueuedConnection);
    connect(ui->btnSave, &QPushButton::clicked, pImageManager, &ImageManager::saveImage, Qt::QueuedConnection);
    connect(ui->btnOpenPreset, &QPushButton::clicked, pImageManager, &ImageManager::loadState, Qt::QueuedConnection);
    connect(ui->btnSavePreset, &QPushButton::clicked, pImageManager, &ImageManager::saveCurrentState, Qt::QueuedConnection);
    connect(ui->btnNashvilleEffectFilter, &QPushButton::clicked, this, &GUI::useNashvilleEffectFilter);
    connect(ui->btnBrightnessContrstFilter, &QPushButton::clicked, this, &GUI::useBrightnessContrastFilter);
    connect(ui->btnNineteenCenturyFilter, &QPushButton::clicked, this, &GUI::useNineteenCenturyFilter);
    connect(ui->btnBlurFilter, &QPushButton::clicked, this, &GUI::useBlurFilter);
    connect(ui->btnVignetteFilter, &QPushButton::clicked, this, &GUI::useVignetteFilter);
    connect(ui->btnVerticalFlip, &QPushButton::clicked, this, &GUI::useVerticalFlip);
    connect(ui->btnHorizontalFlip, &QPushButton::clicked, this, &GUI::useHorizontalFlip);
    connect(ui->btnGrayscaleFilter, &QPushButton::clicked, this, &GUI::useGrayscaleFilter);
    connect(ui->btnSepiaFilter, &QPushButton::clicked, this, &GUI::useSepiaFilter);
    connect(ui->btnExit, &QPushButton::clicked, this, &GUI::close);
    connect(ui->dialAngle, &QDial::valueChanged, this, &GUI::updateRotateParam);

    connect(ui->sliderBlurRadius, &QSlider::valueChanged, this, &GUI::updateBlurParam);
    connect(ui->sliderBrightness, &QSlider::valueChanged, this, &GUI::updateBrightnesstParam);
    connect(ui->sliderContrast, &QSlider::valueChanged, this, &GUI::updateContrastParam);
    connect(ui->btnApply, &QPushButton::clicked, this, &GUI::applyFilter);
    connect(ui->btnBackward, &QPushButton::clicked, pImageManager, &ImageManager::backward);
    connect(ui->btnForward, &QPushButton::clicked, pImageManager, &ImageManager::forward);

#if DEBUG_GUI
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif
}

GUI::~GUI() {
#if DEBUG_GUI
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

    if (pImageManager != nullptr) {
        delete pImageManager;
        pImageManager = nullptr;
    }

    delete ui;

#if DEBUG_GUI
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif
}

void GUI::resizeEvent(QResizeEvent *event) {
#if DEBUG_GUI
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

    if (ui->lblCurrentImage->pixmap() != nullptr) {
        ui->lblCurrentImage->setPixmap(QPixmap::fromImage(pImageManager->getCurrentImage()).scaled(ui->lblCurrentImage->width(), ui->lblCurrentImage->height(), Qt::KeepAspectRatio));
    }

#if DEBUG_GUI
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif
}

void GUI::closeGUI() {
    this->close();
}

void GUI::open() {
    if (pImageManager->uploadImage()) {
        ui->btnGrayscaleFilter->setEnabled(true);
        ui->btnSepiaFilter->setEnabled(true);
        ui->btnHorizontalFlip->setEnabled(true);
        ui->btnVerticalFlip->setEnabled(true);
        ui->dialAngle->setEnabled(true);
        ui->btnApply->setEnabled(true);
        ui->btnForward->setEnabled(true);
        ui->btnBackward->setEnabled(true);
        ui->btnBlurFilter->setEnabled(true);
        ui->btnBrightnessContrstFilter->setEnabled(true);
        ui->btnNashvilleEffectFilter->setEnabled(true);
        ui->btnNineteenCenturyFilter->setEnabled(true);
        ui->btnVignetteFilter->setEnabled(true);
        ui->btnSave->setEnabled(true);
        ui->btnSavePreset->setEnabled(true);

        ui->btnHorizontalFlip->setIcon(QPixmap::fromImage(QImage(":/qss_icons/rc/horizontalFlip.png")).scaled(ui->btnVerticalFlip->width(), ui->btnVerticalFlip->height(), Qt::KeepAspectRatio));
        ui->btnVerticalFlip->setIcon(QPixmap::fromImage(QImage(":/qss_icons/rc/verticalFlip.png")).scaled(ui->btnVerticalFlip->width(), ui->btnVerticalFlip->height(), Qt::KeepAspectRatio));
    }
}

void GUI::initStyleSheet(StyleSheets styleSheet) {
#if DEBUG_GUI
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

    QString styleSheetPath;
    switch(styleSheet) {
    case DARK:
        styleSheetPath = ":/qdarkstyle/dark-blue_style.qss";
        break;
    case NUKE:
        styleSheetPath = ":/qdarkstyle/nuke_style.qss";
        break;
    default:
        styleSheetPath = "";
        break;
    }

    if (!styleSheetPath.isEmpty()) {
        QFile file(styleSheetPath);
        if (!file.exists()) {
            qWarning() << "Unable to set stylesheet, file not found!";
        } else {
            file.open(QFile::ReadOnly | QFile::Text);
            QTextStream textStream(&file);
            qApp->setStyleSheet(textStream.readAll());
            file.close();
        }
    }

#if DEBUG_GUI
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif
}

void GUI::updateProcessedImage(const QImage &image) {
#if DEBUG_GUI
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

    mCurrentImage = QImage(image);
    QPixmap t = QPixmap::fromImage(mCurrentImage, Qt::NoOpaqueDetection);
    ui->lblCurrentImage->setPixmap(QPixmap::fromImage(mCurrentImage).scaled(ui->lblCurrentImage->width(), ui->lblCurrentImage->height(), Qt::KeepAspectRatio));

#if DEBUG_GUI
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif
}

void GUI::updateProcessedIcon(const QImage &image, const int &id) {
#if DEBUG_GUI
    qDebug() << "START";
    QElapsedTimer procTimer; procTimer.start();
#endif

    switch(id) {
    case 0:
        ui->btnBlurFilter->setIcon(QPixmap::fromImage(image).scaled(ui->btnBlurFilter->width(), ui->btnBlurFilter->height(), Qt::KeepAspectRatio));
        break;
    case 1:
        ui->btnBrightnessContrstFilter->setIcon(QPixmap::fromImage(image).scaled(ui->btnBrightnessContrstFilter->width(), ui->btnBrightnessContrstFilter->height(), Qt::KeepAspectRatio));
        break;
    case 2:
        ui->btnNineteenCenturyFilter->setIcon(QPixmap::fromImage(image).scaled(ui->btnNineteenCenturyFilter->width(), ui->btnNineteenCenturyFilter->height(), Qt::KeepAspectRatio));
        break;
    case 3:
        ui->btnNashvilleEffectFilter->setIcon(QPixmap::fromImage(image).scaled(ui->btnNashvilleEffectFilter->width(), ui->btnNashvilleEffectFilter->height(), Qt::KeepAspectRatio));
        break;
    case 4:
        ui->btnVignetteFilter->setIcon(QPixmap::fromImage(image).scaled(ui->btnVignetteFilter->width(), ui->btnVignetteFilter->height(), Qt::KeepAspectRatio));
        break;
    case 5:
        ui->btnGrayscaleFilter->setIcon(QPixmap::fromImage(image).scaled(ui->btnGrayscaleFilter->width(), ui->btnGrayscaleFilter->height(), Qt::KeepAspectRatio));
        break;
    case 6:
        ui->btnSepiaFilter->setIcon(QPixmap::fromImage(image).scaled(ui->btnSepiaFilter->width(), ui->btnSepiaFilter->height(), Qt::KeepAspectRatio));
        break;
    default:
        break;
    }

#if DEBUG_GUI
    qDebug() << "END\tT=" << procTimer.nsecsElapsed() / 1000000.0;
#endif
}

void GUI::useBlurFilter() {
    ui->sliderBrightness->setVisible(false);
    ui->sliderContrast->setVisible(false);
    ui->sliderBlurRadius->setVisible(true);

    ui->lblRadius->setVisible(true);
    ui->lblBrightness->setVisible(false);
    ui->lblContrast->setVisible(false);

    pBlurParam->blurRadius = ui->sliderBlurRadius->value();
    mLastFilterId = (int)BLUR_FILTER;
    emit sendFilterId(mLastFilterId, pBlurParam, false, false);
}

void GUI::useNashvilleEffectFilter() {
    ui->sliderBrightness->setVisible(false);
    ui->sliderContrast->setVisible(false);
    ui->sliderBlurRadius->setVisible(false);

    ui->lblRadius->setVisible(false);
    ui->lblBrightness->setVisible(false);
    ui->lblContrast->setVisible(false);

    mLastFilterId = (int)NASHVILLE_EFFECT_FILTER;
    emit sendFilterId(mLastFilterId, nullptr, false, false);
}

void GUI::useNineteenCenturyFilter() {
    ui->sliderBrightness->setVisible(false);
    ui->sliderContrast->setVisible(false);
    ui->sliderBlurRadius->setVisible(false);

    ui->lblRadius->setVisible(false);
    ui->lblBrightness->setVisible(false);
    ui->lblContrast->setVisible(false);

    mLastFilterId = (int)NINETEEN_CENTURY_FILTER;
    emit sendFilterId(mLastFilterId, nullptr, false, false);
}

void GUI::useBrightnessContrastFilter() {
    ui->sliderBrightness->setVisible(true);
    ui->sliderContrast->setVisible(true);
    ui->sliderBlurRadius->setVisible(false);

    ui->lblRadius->setVisible(false);
    ui->lblBrightness->setVisible(true);
    ui->lblContrast->setVisible(true);

    pBrightnessContrastParam->brightness = ui->sliderBrightness->value();
    pBrightnessContrastParam->contrast = ui->sliderContrast->value();
    mLastFilterId = (int)BRIGHTNESS_CONTRAST_FILTER;
    emit sendFilterId(mLastFilterId, pBrightnessContrastParam, false, false);
}

void GUI::useVignetteFilter() {
    ui->sliderBrightness->setVisible(false);
    ui->sliderContrast->setVisible(false);
    ui->sliderBlurRadius->setVisible(false);

    ui->lblRadius->setVisible(false);
    ui->lblBrightness->setVisible(false);
    ui->lblContrast->setVisible(false);

    mLastFilterId = (int)VIGNETTE_FILTER;
    emit sendFilterId(mLastFilterId, nullptr, false, false);
}

void GUI::useVerticalFlip() {
    pFlippingParam->axe = 0;

    ui->sliderBrightness->setVisible(false);
    ui->sliderContrast->setVisible(false);
    ui->sliderBlurRadius->setVisible(false);

    ui->lblRadius->setVisible(false);
    ui->lblBrightness->setVisible(false);
    ui->lblContrast->setVisible(false);

    mLastFilterId = (int)FLIPPING_FILTER;
    emit sendFilterId(mLastFilterId, pFlippingParam, false, false);
}

void GUI::useHorizontalFlip() {
    pFlippingParam->axe = 1;

    ui->sliderBrightness->setVisible(false);
    ui->sliderContrast->setVisible(false);
    ui->sliderBlurRadius->setVisible(false);

    ui->lblRadius->setVisible(false);
    ui->lblBrightness->setVisible(false);
    ui->lblContrast->setVisible(false);

    mLastFilterId = (int)FLIPPING_FILTER;
    emit sendFilterId(mLastFilterId, pFlippingParam, false, false);
}

void GUI::useGrayscaleFilter() {
    ui->sliderBrightness->setVisible(false);
    ui->sliderContrast->setVisible(false);
    ui->sliderBlurRadius->setVisible(false);

    ui->lblRadius->setVisible(false);
    ui->lblBrightness->setVisible(false);
    ui->lblContrast->setVisible(false);

    mLastFilterId = (int)GRAYSCALE_FILTER;
    emit sendFilterId(mLastFilterId, nullptr, false, false);
}

void GUI::useSepiaFilter() {
    ui->sliderBrightness->setVisible(false);
    ui->sliderContrast->setVisible(false);
    ui->sliderBlurRadius->setVisible(false);

    ui->lblRadius->setVisible(false);
    ui->lblBrightness->setVisible(false);
    ui->lblContrast->setVisible(false);

    mLastFilterId = (int)SEPIA_FILTER;
    emit sendFilterId(mLastFilterId, nullptr, false, false);
}

void GUI::updateBlurParam(const int &blurRadius) {
    pBlurParam->blurRadius = blurRadius;
    mLastFilterId = (int)BLUR_FILTER;
    emit sendFilterId(mLastFilterId, pBlurParam, false, false);
}

void GUI::updateBrightnesstParam(const int &brightness) {
    pBrightnessContrastParam->brightness = brightness;
    mLastFilterId = (int)BRIGHTNESS_CONTRAST_FILTER;
    emit sendFilterId(mLastFilterId, pBrightnessContrastParam, false, false);
}

void GUI::updateContrastParam(const int &contrast) {
    pBrightnessContrastParam->contrast = contrast;
    mLastFilterId = (int)BRIGHTNESS_CONTRAST_FILTER;
    emit sendFilterId(mLastFilterId, pBrightnessContrastParam, false, false);
}

void GUI::updateRotateParam(const int &angle) {
    ui->sliderBrightness->setVisible(false);
    ui->sliderContrast->setVisible(false);
    ui->sliderBlurRadius->setVisible(false);

    ui->lblRadius->setVisible(false);
    ui->lblBrightness->setVisible(false);
    ui->lblContrast->setVisible(false);

    pRotateParam->angle = angle;
    mLastFilterId = (int)ROTATE_FILTER;
    emit sendFilterId(mLastFilterId, pRotateParam, false, false);
}

void GUI::applyFilter() {
    qDebug() << "mLastFilterId=" << mLastFilterId;
    if (mLastFilterId == -1) {

    } else {
        switch(mLastFilterId) {
        case 0:
            emit sendFilterId(mLastFilterId, pBlurParam, true, true);
            break;
        case 1:
            emit sendFilterId(mLastFilterId, pBrightnessContrastParam, true, true);
            break;
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
            emit sendFilterId(mLastFilterId, nullptr, true, true);
            break;
        case 7:
            emit sendFilterId(mLastFilterId, pFlippingParam, true, true);
            break;
        case 8:
            emit sendFilterId(mLastFilterId, pRotateParam, true, true);
            break;
        default:
            break;
        }
    }
}
