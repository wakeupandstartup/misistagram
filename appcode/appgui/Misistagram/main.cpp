#include "gui.h"
#include <QApplication>

int main(int argc, char *argv[]) {
    qSetMessagePattern("%{time h:mm:ss.zzz}  %{if-debug}D%{endif}%{if-info}I%{endif}%{if-warning}W%{endif}%{if-critical}C%{endif}%{if-fatal}F%{endif}  %{threadid} %{function}\t\t%{message}");

    QApplication a(argc, argv);
    GUI w;
    w.show();

    return a.exec();
}
