#ifndef IMAGEMANAGER_H
#define IMAGEMANAGER_H

#pragma once

#include <QObject>
#include <QString>
#include <QImage>
#include <QFile>
#include <QFileDialog>
#include <QDebug>
#include <QElapsedTimer>

enum CV_FORMATS {
    CV_8UC1 = 0,
    CV_8UC3 = 16,
    CV_8UC4 = 24
};

#include "MisisAPI.h"

class ImageManager : public QObject {
    Q_OBJECT
public:
    ImageManager();
    ~ImageManager();

signals:
    void sendProcessedImage(const QImage &image);
    void sendProcessedIcon(const QImage &image, const int &id);

public:
    QImage getCurrentImage() const;

private:
    void initializeImageData(ImageData &src, ImageData &dst);
    void createImageData(ImageData &imageData, QImage &qImage);
    void createQImage(QImage &qImage, ImageData &imageData);
    int getCvFormatFromQt(QImage::Format qFormat) const;
    QImage::Format getQtFormatFromCv(int cvFormat);

public:
    bool uploadImage();
    void saveImage();
    void applyFilter(const int &filterId, void* parameters, const bool &isTotal, const bool &toHistory);
    void totalApplyFilter(const int &filterId, void* parameters);
    void addFilterToHistory(FILTER_IDS id, void* parameters);

    void backward();
    void forward();
    void undoHistory();
    void reproduceFromInput();
    void saveCurrentState(/*std::string& path*/);
    void loadState(/*std::string& path*/);


public:
    std::vector<FILTER_IDS> mOperations;
    std::vector<uint8_t>    mParameters;
    QImage mInputImage;
    QImage mPrevImage;
    QImage mCurrentImage;
    ImageData mDesc;
    ImageData mDesc2;
    int mCurrentHitoryPtr = 0;
};

#endif // IMAGEMANAGER_H
