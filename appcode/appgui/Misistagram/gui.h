#ifndef GUI_H
#define GUI_H

#include <QMainWindow>
#include <QElapsedTimer>

#include <imagemanager.h>

enum StyleSheets {
    DARK,
    NUKE,
    DEFAULT
};

namespace Ui {
class GUI;
}

class GUI : public QMainWindow {
    Q_OBJECT

signals:
    void sendFilterId(const int &id, void *param, const bool &isTotal, const bool &toHistory);
    void backward();
    void forward();

public:
    explicit GUI(QWidget *parent = 0);
    ~GUI();
    void resizeEvent(QResizeEvent *event);
    void closeGUI();

private:
    void open();
    void initStyleSheet(StyleSheets styleSheet);
    void updateProcessedImage(const QImage &image);
    void updateProcessedIcon(const QImage &image, const int &id);

private:
    void useBlurFilter();
    void useNashvilleEffectFilter();
    void useNineteenCenturyFilter();
    void useBrightnessContrastFilter();
    void useBloomFilter();
    void useVignetteFilter();
    void useVerticalFlip();
    void useHorizontalFlip();
    void useGrayscaleFilter();
    void useSepiaFilter();

private:
    void updateBlurParam(const int &blurRadius);
    void updateBrightnesstParam(const int &brightness);
    void updateContrastParam(const int &contrast);
    void updateRotateParam(const int &angle);

private:
    void applyFilter();
    void updateFocusingStatus(QWidget *widget);

private:
    Ui::GUI *ui;
    ImageManager *pImageManager{nullptr};
    QImage mCurrentImage;
    FlippingParam *pFlippingParam{nullptr};
    BlurParam *pBlurParam{nullptr};
    BrigtnessContrastParam *pBrightnessContrastParam{nullptr};
    RotateParam *pRotateParam{nullptr};

    int mLastFilterId = -1;
};

#endif // GUI_H
