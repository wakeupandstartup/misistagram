#-------------------------------------------------
#
# Project created by QtCreator 2017-04-10T13:45:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Misistagram
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


RESOURCES += \
    style.qrc

SOURCES += main.cpp\
        gui.cpp \
    imagemanager.cpp

HEADERS  += gui.h \
    imagemanager.h

FORMS    += gui.ui

INCLUDEPATH += -L "./../../headers"

CONFIG(debug, debug|release){
    message(Debug build)
    DESTDIR = L./../../../binDebug
    LIBS += -L./../../libDebug/ -lMisisCore
}
CONFIG(release,debug|release){
    message(Release build)
    DESTDIR = L./../../../binRelease
    LIBS += -L./../../libRelease/ -lMisisCore
}

