#ifndef MISISCORE_H
#define MISISCORE_H


#include <cinttypes>
#include <memory>
#include <vector>
#include <MisisAPI.h>
#include <../headers/Filters.h>
#include <opencv2\opencv.hpp>


namespace Misistragram
{

	class MisistagramImageProcessor
	{
	public:
		MisistagramImageProcessor()
		{
			/// ADD NEW FILERS HERE!
			mFilters.resize(FILTER_COUNT);
			mFilters[BLUR_FILTER].reset(new BlurFilter);
			mFilters[NASHVILLE_EFFECT_FILTER].reset(new NashvilleFilter);
			mFilters[BRIGHTNESS_CONTRAST_FILTER].reset(new BrightnessContrast);
			mFilters[NINETEEN_CENTURY_FILTER].reset(new NineteenCenturyFilter);
 		  mFilters[VIGNETTE_FILTER].reset(new VignetteFilter);
			mFilters[ROTATE_FILTER].reset(new RotateFilter);
      mFilters[GRAYSCALE_FILTER].reset(new GrayscaleFilter);
      mFilters[FLIPPING_FILTER].reset(new FlippingFilter);
      mFilters[SEPIA_FILTER].reset(new SepiaFilter);

		}

		void processImage(FILTER_IDS filter, void* parameters, ImageData* img);
		void getImage(ImageData* img);

	private:
		std::vector<std::shared_ptr<Misistragram::Filter>> mFilters;	
		cv::Mat mLastResult;

	};

}


#endif