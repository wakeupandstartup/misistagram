#ifndef MISISTAGRAMFILTERS_H
#define MISISTAGRAMFILTERS_H

#include <MisisAPI.h>

namespace cv
{
	class Mat;
}

namespace Misistragram
{
	class Filter
	{
	public:
		virtual void processImage(cv::Mat& img, void* parameters) = 0;
	};

	class BlurFilter : public Filter
	{
	public:
		~BlurFilter() {};
		virtual void processImage(cv::Mat& img, void* parameters) override;
	};

	class NashvilleFilter : public Filter
	{
	public:
		~NashvilleFilter() {};
		virtual void processImage(cv::Mat& img, void* parameters) override;
	};

	class NineteenCenturyFilter : public Filter
	{
	public:
		~NineteenCenturyFilter() {};
		virtual void processImage(cv::Mat& img, void* parameters) override;
	};

	class BrightnessContrast : public Filter
	{
	public:
		~BrightnessContrast() {};
		virtual void processImage(cv::Mat& img, void* parameters) override;
	};

	class BloomFilter : public Filter
	{
	public:
		~BloomFilter() {};
		virtual void processImage(cv::Mat& img, void* parameters) override;
	};

  class VignetteFilter : public Filter
  {
  public:
    ~VignetteFilter() {};
    virtual void processImage(cv::Mat& ing, void* parameters) override;
  };

  class RotateFilter : public Filter
  {
  public:
    ~RotateFilter() {};
    virtual void processImage(cv::Mat& img, void* parameters) override;
  };

  class GrayscaleFilter : public Filter
  {
  public:
    ~GrayscaleFilter() {};
    virtual void processImage(cv::Mat& ing, void* parameters) override;
  };

  class FlippingFilter : public Filter
  {
  public:
    ~FlippingFilter() {};
    virtual void processImage(cv::Mat& ing, void* parameters) override;
  };

  class SepiaFilter : public Filter
  {
  public:
    ~SepiaFilter() {};
    virtual void processImage(cv::Mat& ing, void* parameters) override;
  };

}

#endif