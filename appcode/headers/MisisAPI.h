#ifndef MISISTAGRAM_API
#define MISISTAGRAM_API
#include <cinttypes>


/**
	Filter list
*/
enum FILTER_IDS {
	BLUR_FILTER,
	BRIGHTNESS_CONTRAST_FILTER,
	NINETEEN_CENTURY_FILTER,
	NASHVILLE_EFFECT_FILTER,
	VIGNETTE_FILTER,
    GRAYSCALE_FILTER,
    SEPIA_FILTER,
	FLIPPING_FILTER,
	ROTATE_FILTER,

	FILTER_COUNT
};

/**
	Filter parameters
*/
struct BlurParam
{
	BlurParam() {}
	float blurRadius;
};

struct BrigtnessContrastParam
{
	BrigtnessContrastParam() {}
	float brightness;
	float contrast;
};

struct RotateParam
{
	RotateParam() {}
	double angle;
};

struct FlippingParam
{
  FlippingParam() {}
  int axe; // x - 0, y - 1
};


/**
	Image holding structure
*/
struct ImageData
{
	ImageData() {}
	uint32_t mWidth;
	uint32_t mHeight;
	uint8_t* mData;
	uint32_t mBytesPerLine;
	int mCvFormat;
	int mChannels;
};


extern "C"
{
	/**
		Image manipulation API
	*/
	__declspec(dllexport) void createBackEnd();


	__declspec(dllexport) void modifyImage(ImageData* data, FILTER_IDS id, void* parameters);

	__declspec(dllexport) void retrieveLastImage(ImageData* data);


	__declspec(dllexport) void releaseBackEnd();

	/**
		Get filter parameter size in bytes
	*/
	__declspec(dllexport) uint32_t getParameterSize(FILTER_IDS);

}

/////////////////////////////////////////////

#endif

