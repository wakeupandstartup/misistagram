#include <../headers/Filters.h>
#include <opencv2\opencv.hpp>


using namespace cv;
using namespace Misistragram;

void BlurFilter::processImage(Mat& img, void* parameters)
{
	BlurParam* param = reinterpret_cast<BlurParam*>(parameters);
	GaussianBlur(img, img, Size(), param->blurRadius);
}


void curve(Mat& img, float input, float output)
{
	
	float k = (255.0f - output) / (255.0f - input);


	for (int32_t i = 0; i < img.rows; ++i)
	{
		for (int32_t j = 0; j < img.cols; ++j)
		{
			float vl = img.at<unsigned char>(i, j) - input;
			vl = vl * k + output;
			vl = max(output, min(255.0f, vl));
			img.at<unsigned char>(i, j) = (unsigned char)vl;
		}
	}
}

void gamma(Mat& img, float gamma)
{
	Mat temp;
	img.convertTo(temp, CV_32FC3, 1.0f / 255.0f);
	pow(temp, gamma, temp);
	temp.convertTo(img, CV_8UC3, 255.0f);
}

void brightnessContrast(Mat& img, float brighness, float contrast)
{
	Mat temp;
	img.convertTo(temp, CV_32FC3, 1.0f / 255.0f);
	temp = (1.0f + contrast / 255.0f) * temp + Scalar(brighness / 255.0f, brighness / 255.0f, brighness / 255.0f);
	temp.convertTo(img, CV_8UC3, 255.0f);
}

// Helper function to calculate the distance between 2 points.
double dist(CvPoint a, CvPoint b)
{
  return sqrt(pow((double)(a.x - b.x), 2) + pow((double)(a.y - b.y), 2));
}

// Helper function that computes the longest distance from the edge to the center point.
double getMaxDisFromCorners(const cv::Size& imgSize, const cv::Point& center)
{
  // given a rect and a line
  // get which corner of rect is farthest from the line

  std::vector<cv::Point> corners(4);
  corners[0] = cv::Point(0, 0);
  corners[1] = cv::Point(imgSize.width, 0);
  corners[2] = cv::Point(0, imgSize.height);
  corners[3] = cv::Point(imgSize.width, imgSize.height);

  double maxDis = 0;
  for (int i = 0; i < 4; ++i)
  {
    double dis = dist(corners[i], center);
    if (maxDis < dis)
      maxDis = dis;
  }

  return maxDis;
}

// Helper function that creates a gradient image.   
// firstPt, radius and power, are variables that control the artistic effect of the filter.
void generateGradient(cv::Mat& mask)
{
  cv::Point firstPt = cv::Point(mask.size().width / 2, mask.size().height / 2);
  double radius = 1.0;
  double power = 0.8;

  double maxImageRad = radius * getMaxDisFromCorners(mask.size(), firstPt);

  mask.setTo(cv::Scalar(1));
  for (int i = 0; i < mask.rows; i++)
  {
    for (int j = 0; j < mask.cols; j++)
    {
      double temp = dist(firstPt, cv::Point(j, i)) / maxImageRad;
      temp = temp * power;
      double temp_s = pow(cos(temp), 4);
      mask.at<double>(i, j) = temp_s;
    }
  }
}

void NashvilleFilter::processImage(cv::Mat& img, void* parameters)
{
	Mat overlay = Mat::zeros(img.size(), CV_8UC3);
	overlay += Scalar(172, 216, 246);


	std::vector<Mat> perChannel(3);
	split(img, perChannel);
	curve(perChannel[1], 0, 37);
	curve(perChannel[0], 0, 133);
	merge(perChannel, img);
	gamma(img, 0.8f);
	
	brightnessContrast(img, 6, 21);

	split(img, perChannel);
	curve(perChannel[1], 18, 0);
	curve(perChannel[0], 48, 0);
	merge(perChannel, img);
	brightnessContrast(img, -8, 23);

	split(img, perChannel);
	curve(perChannel[2], 0, 4);
	curve(perChannel[0], 0, 14);
	merge(perChannel, img);

	multiply(img, overlay, img, 1.0f / 255.0f);
}

void NineteenCenturyFilter::processImage(cv::Mat& img, void* parameters)
{
	std::vector<Mat> perChannel(3);
	split(img, perChannel);
	curve(perChannel[0], 0, 44);
	curve(perChannel[2], 0, 91);
	merge(perChannel, img);
	brightnessContrast(img, 15, 17);
	gamma(img, 1.2);
}

void BrightnessContrast::processImage(cv::Mat& img, void* parameters)
{
	BrigtnessContrastParam* param = reinterpret_cast<BrigtnessContrastParam*>(parameters);
	
	brightnessContrast(img, param->brightness, param->contrast);
}

void VignetteFilter::processImage(cv::Mat& img, void* parameters)
{
  cv::Mat maskImg(img.size(), CV_64F);
  generateGradient(maskImg);
  cv::Mat labImg(img.size(), CV_8UC3);
  cv::cvtColor(img, labImg, CV_BGR2Lab);

  for (int row = 0; row < labImg.size().height; row++)
  {
    for (int col = 0; col < labImg.size().width; col++)
    {
      cv::Vec3b value = labImg.at<cv::Vec3b>(row, col);
      value.val[0] *= maskImg.at<double>(row, col);
      labImg.at<cv::Vec3b>(row, col) = value;
    }
  }

  cv::Mat output;
  cv::cvtColor(labImg, output, CV_Lab2BGR);

  output.copyTo(img);
}

void RotateFilter::processImage(cv::Mat& img, void* parameters)
{
	int imgRows = img.rows;
	int imgCols = img.cols;
  RotateParam* param = reinterpret_cast<RotateParam*>(parameters);
  double angle = param->angle;

  cv::Point2f center(img.cols / 2.0, img.rows / 2.0);
  cv::Mat rot = cv::getRotationMatrix2D(center, angle, 1.0);

  cv::Rect bbox = cv::RotatedRect(center, img.size(), angle).boundingRect();

  rot.at<double>(0, 2) += bbox.width / 2.0 - center.x;
  rot.at<double>(1, 2) += bbox.height / 2.0 - center.y;

  cv::Mat dst;
  cv::warpAffine(img, dst, rot, bbox.size());
  img.release();
  dst.copyTo(img);
}

void GrayscaleFilter::processImage(cv::Mat& img, void* parameters)
{
  cv::Mat grayMat;
  cv::cvtColor(img, grayMat, CV_BGR2GRAY);
  img.release();
  grayMat.copyTo(img);
}

void FlippingFilter::processImage(cv::Mat& img, void* parameters)
{
  FlippingParam* param = reinterpret_cast<FlippingParam*>(parameters);
  int axe = param->axe;

  cv::Mat dst;
  cv::flip(img, dst, axe);

  img.release();
  dst.copyTo(img);
}

void SepiaFilter::processImage(cv::Mat& img, void* parameters)
{
  Mat_<float> sepia(3, 3);
  sepia << .131, .534, .272,
           .168, .686, .349,
           .189, .769, .393;
  Mat dst;
  cv::transform(img, dst, sepia);

  img.release();
  dst.copyTo(img);
}