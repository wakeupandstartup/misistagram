#include <opencv2\opencv.hpp>
#include "MisistagramCore.h"
#include <iostream>
#include <cassert>

using namespace cv;
using namespace Misistragram;


Mat toMat(const ImageData& img)
{
	assert(img.mCvFormat != -1);
	cv::Mat retValue = Mat(img.mHeight, img.mWidth, img.mCvFormat, const_cast<uchar*>(img.mData), img.mBytesPerLine).clone();
	if (retValue.type() == CV_8UC1) {
		cv::cvtColor(retValue, retValue, CV_GRAY2BGR);
	}
	return retValue;
}

void toImg(ImageData* img, Mat& cvImg)
{
	if (cvImg.cols != img->mWidth || cvImg.rows != img->mHeight)
	{
		return;
	}
	memcpy(img->mData, (const uchar *)cvImg.data, sizeof(uint8_t) * img->mWidth * img->mHeight * cvImg.channels());
}

void MisistagramImageProcessor::processImage(FILTER_IDS filter, void* parameters, ImageData* img)
{
	if (filter >= FILTER_COUNT)
	{
		return;
	}
	Mat cvImage = toMat(*img);
	mFilters[filter]->processImage(cvImage, parameters);
	img->mWidth = cvImage.cols;
	img->mHeight = cvImage.rows;
	img->mBytesPerLine = cvImage.step;
	img->mChannels = cvImage.channels();
	img->mCvFormat = cvImage.type();
	cvImage.copyTo(mLastResult);
}

void MisistagramImageProcessor::getImage(ImageData* img)
{
	if (!mLastResult.empty())
	{
		toImg(img, mLastResult);
	}
}

