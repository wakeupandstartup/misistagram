#include "MisisAPI.h"
#include "MisistagramCore.h"

Misistragram::MisistagramImageProcessor* prc = nullptr;


void createBackEnd()
{
        prc = new Misistragram::MisistagramImageProcessor();
};

void modifyImage(ImageData* data, FILTER_IDS id, void* parameters)
{
	assert(prc != nullptr);
	if (prc == nullptr)
	{
		return;
	}
    prc->processImage(id, parameters, data);
};

void retrieveLastImage(ImageData* data)
{
	assert(data != nullptr);
	assert(data->mData != nullptr);
	if (data == nullptr || data->mData == nullptr)
	{
		return;
	}
	prc->getImage(data);
};

void releaseBackEnd()
{
        delete prc;
        prc = nullptr;
};

uint32_t getParameterSize(FILTER_IDS id)
{
	switch (id)
	{
	case BRIGHTNESS_CONTRAST_FILTER:
		return sizeof(BrigtnessContrastParam);
		break;
	case NINETEEN_CENTURY_FILTER:
		return 0;
		break;
	case NASHVILLE_EFFECT_FILTER:
		return 0;
		break;
	case BLUR_FILTER:
		return sizeof(BlurParam);
		break;
	case VIGNETTE_FILTER:
		return 0;
		break;
  case ROTATE_FILTER:
    return sizeof(RotateParam);
    break;
  case GRAYSCALE_FILTER:
    return 0;
    break;
  case FLIPPING_FILTER:
    return sizeof(FlippingParam);
    break;
  case SEPIA_FILTER:
    return 0;
    break;
	default:
		return 0;
		break;
	}

	return 0;
}