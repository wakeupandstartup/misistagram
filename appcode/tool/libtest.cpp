#include <opencv2\opencv.hpp>
#include <MisisAPI.h>
#include <iostream>
#include <Windows.h>

using namespace cv;

#define TEST_FILTERS
//#define PREPARE_TEST



void writeFilteredData(ImageData data, std::string filterTest)
{
	FILE* blob = fopen(filterTest.c_str(), "wb");

	fwrite(&data, sizeof(ImageData), 1, blob);
	fwrite(data.mData, sizeof(uint8_t) * data.mWidth * data.mHeight * data.mChannels, 1, blob);
	fclose(blob);
}


void readFilteredData(ImageData& data, std::string filterTest)
{
	if (data.mData != nullptr)
	{
		delete[] data.mData;
		data.mData = nullptr;
	}
	FILE* blob = fopen(filterTest.c_str(), "rb");
	fread(&data, sizeof(ImageData), 1, blob);
	data.mData = new uint8_t[data.mWidth * data.mHeight * data.mChannels];
	fread(data.mData, sizeof(uint8_t) * data.mWidth * data.mHeight * data.mChannels, 1, blob);
	fclose(blob);
}

void prepateImageData(ImageData& desc, Mat& img)
{
	desc.mHeight = img.size().height;
	desc.mWidth = img.size().width;
	desc.mData = img.data;
	desc.mBytesPerLine = img.step;
	desc.mChannels = img.channels();
	desc.mCvFormat = img.type();
}

bool compareTwoImdatas(ImageData& desc, ImageData& desc2)
{	
	if (desc.mBytesPerLine != desc2.mBytesPerLine) return false;
	if (desc.mChannels != desc2.mChannels) return false;
	if (desc.mCvFormat != desc2.mCvFormat) return false;
	if (desc.mHeight != desc2.mHeight) return false;
	if (desc.mWidth != desc2.mWidth) return false;

	if (memcmp(desc.mData, desc2.mData, desc.mWidth * desc.mHeight * desc.mChannels)) return false;
	return true;
}


void PRINT_SUCCESS(std::string text)
{
	std::cout << "\nTesting filter: " << text << " ";
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, 2);
	std::cout << "OK" << std::endl;
	SetConsoleTextAttribute(hConsole, 7);
}

void PRINT_FAILURE(std::string text)
{
	std::cout << "\nTesting filter: " << text << " ";
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, 12);
	std::cout << "FAILURE" << std::endl;
	SetConsoleTextAttribute(hConsole, 7);
}

Mat getCvMat(const ImageData& img)
{
	assert(img.mCvFormat != -1);
	cv::Mat retValue = Mat(img.mHeight, img.mWidth, img.mCvFormat, const_cast<uchar*>(img.mData), img.mBytesPerLine).clone();
	if (retValue.type() == CV_8UC1) {
		cv::cvtColor(retValue, retValue, CV_GRAY2BGR);
	}
	return retValue;
}








#ifdef PREPARE_TEST



int main()
{
	createBackEnd();



	Mat img, image = imread("../../appdata/trump_test.jpg");
	Mat result;
	ImageData desc;

	/**
	SEPIA_FILTER test
	*/
	std::string imname = "../testData/SEPIA_FILTER.jpg";
	std::string blobname = "../testData/SEPIA_FILTER.blob";
	image.copyTo(img);
	prepateImageData(desc, img);
	modifyImage(&desc, SEPIA_FILTER, nullptr);
	retrieveLastImage(&desc);
	result = getCvMat(desc);
	writeFilteredData(desc, blobname);
	imwrite(imname, result);	


	/**
	NASHVILLE_EFFECT_FILTER test
	*/
	imname = "../testData/NASHVILLE_EFFECT_FILTER.jpg";
	blobname = "../testData/NASHVILLE_EFFECT_FILTER.blob";
	image.copyTo(img);
	prepateImageData(desc, img);
	modifyImage(&desc, NASHVILLE_EFFECT_FILTER, nullptr);
	retrieveLastImage(&desc);
	result = getCvMat(desc);
	writeFilteredData(desc, blobname);
	imwrite(imname, result);

	/**
	VIGNETTE_FILTER test
	*/
	imname = "../testData/VIGNETTE_FILTER.jpg";
	blobname = "../testData/VIGNETTE_FILTER.blob";
	image.copyTo(img);
	prepateImageData(desc, img);
	modifyImage(&desc, VIGNETTE_FILTER, nullptr);
	retrieveLastImage(&desc);
	result = getCvMat(desc);
	writeFilteredData(desc, blobname);
	imwrite(imname, result);

	/**
  GRAYSCALE_FILTER test
	*/
	imname = "../testData/GRAYSCALE_FILTER.jpg";
	blobname = "../testData/GRAYSCALE_FILTER.blob";
	image.copyTo(img);
	prepateImageData(desc, img);

	modifyImage(&desc, GRAYSCALE_FILTER, nullptr);
	retrieveLastImage(&desc);
	result = getCvMat(desc);
	writeFilteredData(desc, blobname);
	imwrite(imname, result);

	/**
  BLUR_FILTER test
	*/
	imname = "../testData/BLUR_FILTER.jpg";
	blobname = "../testData/BLUR_FILTER.blob";
	image.copyTo(img);
	prepateImageData(desc, img);
	BlurParam blurprm;
	blurprm.blurRadius = 4;
	modifyImage(&desc, BLUR_FILTER, &blurprm);
	retrieveLastImage(&desc);
	result = getCvMat(desc);
	writeFilteredData(desc, blobname);
	imwrite(imname, result);

  /**
  BRIGHTNESS_CONTRAST_FILTER test
  */
  imname = "../testData/BRIGHTNESS_CONTRAST_FILTER.jpg";
  blobname = "../testData/BRIGHTNESS_CONTRAST_FILTER.blob";
  image.copyTo(img);
  prepateImageData(desc, img);
  BrigtnessContrastParam bcprm;
  bcprm.brightness = 20;
  bcprm.contrast = 100;
  modifyImage(&desc, BRIGHTNESS_CONTRAST_FILTER, &bcprm);
  retrieveLastImage(&desc);
  result = getCvMat(desc);
  writeFilteredData(desc, blobname);
  imwrite(imname, result);

  /**
  NINETEEN_CENTURY_FILTER test
  */
  imname = "../testData/NINETEEN_CENTURY_FILTER.jpg";
  blobname = "../testData/NINETEEN_CENTURY_FILTER.blob";
  image.copyTo(img);
  prepateImageData(desc, img);
  modifyImage(&desc, NINETEEN_CENTURY_FILTER, nullptr);
  retrieveLastImage(&desc);
  result = getCvMat(desc);
  writeFilteredData(desc, blobname);
  imwrite(imname, result);

  /**
  FLIPPING_FILTER test
  */
  imname = "../testData/FLIPPING_FILTER.jpg";
  blobname = "../testData/FLIPPING_FILTER.blob";
  image.copyTo(img);
  prepateImageData(desc, img);
  FlippingParam flipprm;
  flipprm.axe = 0;
  modifyImage(&desc, FLIPPING_FILTER, &flipprm);
  retrieveLastImage(&desc);
  result = getCvMat(desc);
  writeFilteredData(desc, blobname);
  imwrite(imname, result);

  /**
  ROTATE_FILTER test
  */
  imname = "../testData/ROTATE_FILTER.jpg";
  blobname = "../testData/ROTATE_FILTER.blob";
  image.copyTo(img);
  prepateImageData(desc, img);
  RotateParam rotateprm;
  rotateprm.angle = 45;
  modifyImage(&desc, ROTATE_FILTER, &rotateprm);
  desc.mData = new uint8_t[desc.mChannels * desc.mHeight * desc.mWidth];
  retrieveLastImage(&desc);
  result = getCvMat(desc);
  writeFilteredData(desc, blobname);
  imwrite(imname, result);
  delete[] desc.mData;
	waitKey(0);
	releaseBackEnd();
}



#endif 


#ifdef TEST_FILTERS
int main()
{
	std::cout << "\n\n\n\n============  FILTER AUTO TESTs  ============\n\n\n";


	createBackEnd();

	Mat img, image = imread("../../appdata/trump_test.jpg");
	Mat result;
	ImageData desc;
	ImageData compDesc;
	compDesc.mData = nullptr;

	/**
	SEPIA_FILTER test
	*/
	std::string imname = "../testData/SEPIA_FILTER.jpg";
	std::string blobname = "../testData/SEPIA_FILTER.blob";
	image.copyTo(img);
	prepateImageData(desc, img);
	modifyImage(&desc, SEPIA_FILTER, nullptr);
	retrieveLastImage(&desc);
	readFilteredData(compDesc, blobname);
	if (compareTwoImdatas(desc, compDesc))
	{
		PRINT_SUCCESS("SEPIA_FILTER");
	}
	else
	{
		PRINT_FAILURE("SEPIA_FILTER");
	}


	/**
	NASHVILLE_EFFECT_FILTER test
	*/
	imname = "../testData/NASHVILLE_EFFECT_FILTER.jpg";
	blobname = "../testData/NASHVILLE_EFFECT_FILTER.blob";
	image.copyTo(img);
	prepateImageData(desc, img);
	modifyImage(&desc, NASHVILLE_EFFECT_FILTER, nullptr);
	retrieveLastImage(&desc);
	readFilteredData(compDesc, blobname);
	if (compareTwoImdatas(desc, compDesc))
	{
		PRINT_SUCCESS("NASHVILLE_EFFECT_FILTER");
	}
	else
	{
		PRINT_FAILURE("NASHVILLE_EFFECT_FILTER");
	}

	/**
	VIGNETTE_FILTER test
	*/
	imname = "../testData/VIGNETTE_FILTER.jpg";
	blobname = "../testData/VIGNETTE_FILTER.blob";
	image.copyTo(img);
	prepateImageData(desc, img);
	modifyImage(&desc, VIGNETTE_FILTER, nullptr);
	retrieveLastImage(&desc);
	readFilteredData(compDesc, blobname);
	if (compareTwoImdatas(desc, compDesc))
	{
		PRINT_SUCCESS("VIGNETTE_FILTER");
	}
	else
	{
		PRINT_FAILURE("VIGNETTE_FILTER");
	}

	/**
	GRAYSCALE_FILTER test
	*/
	imname = "../testData/GRAYSCALE_FILTER.jpg";
	blobname = "../testData/GRAYSCALE_FILTER.blob";
	image.copyTo(img);
	prepateImageData(desc, img);
	modifyImage(&desc, GRAYSCALE_FILTER, nullptr);
	retrieveLastImage(&desc);
	readFilteredData(compDesc, blobname);
	if (compareTwoImdatas(desc, compDesc))
	{
		PRINT_SUCCESS("GRAYSCALE_FILTER");
	}
	else
	{
		PRINT_FAILURE("GRAYSCALE_FILTER");
	}

	/**
	BLUR_FILTER test
	*/
	imname = "../testData/BLUR_FILTER.jpg";
	blobname = "../testData/BLUR_FILTER.blob";
	image.copyTo(img);
	prepateImageData(desc, img);
	BlurParam blurprm;
	blurprm.blurRadius = 4;
	modifyImage(&desc, BLUR_FILTER, &blurprm);
	retrieveLastImage(&desc);
	readFilteredData(compDesc, blobname);
	if (compareTwoImdatas(desc, compDesc))
	{
		PRINT_SUCCESS("BLUR_FILTER");
	}
	else
	{
		PRINT_FAILURE("BLUR_FILTER");
	}

  /**
  BRIGHTNESS_CONTRAST_FILTER test
  */
  imname = "../testData/BRIGHTNESS_CONTRAST_FILTER.jpg";
  blobname = "../testData/BRIGHTNESS_CONTRAST_FILTER.blob";
  image.copyTo(img);
  prepateImageData(desc, img);
  BrigtnessContrastParam bcprm;
  bcprm.brightness = 20;
  bcprm.contrast = 100;
  modifyImage(&desc, BRIGHTNESS_CONTRAST_FILTER, &bcprm);
  retrieveLastImage(&desc);
  readFilteredData(compDesc, blobname);
  if (compareTwoImdatas(desc, compDesc))
  {
    PRINT_SUCCESS("BRIGHTNESS_CONTRAST_FILTER");
  }
  else
  {
    PRINT_FAILURE("BRIGHTNESS_CONTRAST_FILTER");
  }

  /**
  NINETEEN_CENTURY_FILTER test
  */
  imname = "../testData/NINETEEN_CENTURY_FILTER.jpg";
  blobname = "../testData/NINETEEN_CENTURY_FILTER.blob";
  image.copyTo(img);
  prepateImageData(desc, img);
  modifyImage(&desc, NINETEEN_CENTURY_FILTER, nullptr);
  retrieveLastImage(&desc);
  readFilteredData(compDesc, blobname);
  if (compareTwoImdatas(desc, compDesc))
  {
    PRINT_SUCCESS("NINETEEN_CENTURY_FILTER");
  }
  else
  {
    PRINT_FAILURE("NINETEEN_CENTURY_FILTER");
  }

  /**
  FLIPPING_FILTER test
  */
  imname = "../testData/FLIPPING_FILTER.jpg";
  blobname = "../testData/FLIPPING_FILTER.blob";
  image.copyTo(img);
  prepateImageData(desc, img);
  FlippingParam flipprm;
  flipprm.axe = 0;
  modifyImage(&desc, FLIPPING_FILTER, &flipprm);
  retrieveLastImage(&desc);
  readFilteredData(compDesc, blobname);
  if (compareTwoImdatas(desc, compDesc))
  {
    PRINT_SUCCESS("FLIPPING_FILTER");
  }
  else
  {
    PRINT_FAILURE("FLIPPING_FILTER");
  }

  /**
  ROTATE_FILTER test
  */
  imname = "../testData/ROTATE_FILTER.jpg";
  blobname = "../testData/ROTATE_FILTER.blob";
  image.copyTo(img);
  prepateImageData(desc, img);
  RotateParam rotateprm;
  rotateprm.angle = 45;
  modifyImage(&desc, ROTATE_FILTER, &rotateprm);
  desc.mData = new uint8_t[desc.mChannels * desc.mHeight * desc.mWidth];
  retrieveLastImage(&desc);
  readFilteredData(compDesc, blobname);
  if (compareTwoImdatas(desc, compDesc))
  {
    PRINT_SUCCESS("ROTATE_FILTER");
  }
  else
  {
    PRINT_FAILURE("ROTATE_FILTER");
  }
  delete[] desc.mData;
	releaseBackEnd();
}




#endif 






